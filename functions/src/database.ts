import * as firebase from 'firebase-admin'
import * as functions from 'firebase-functions';

export default class Database {
  basepath: string
  db: any

  constructor(basepath: string='', firebaseApp=firebase.app()) {
    this.db = firebaseApp.firestore()
    this.basepath = basepath
  }
  
  collection(path) {
    return this.db.collection(`${this.basepath}/${path}`)
  }

}
