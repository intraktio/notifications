import * as functions from 'firebase-functions';
import ProviderFactory from './provider-factory';
import Provider from './provider';
import Authentication from './authenticate';
import Database from './database'
import 'es6-promise';

export default class NotificationService {
    private db: Database
    private settings: any
    private emailProvider: Provider
    private smsProvider: Provider

    constructor(db: Database, settings: any=null) {
        this.db = db
        this.settings = settings
    }

    async withDefaultSettings() {
        this.settings = await this.getDefaultSettings()
        return this
    }

    getDefaultSettings = async () => {
        const doc = await this.db.collection('').doc('settings').get()
        return doc.data()
    }

    /**
     * @throws error
     */
    createProvider = (providerType: string) => {
        const pFactory = new ProviderFactory()
        return pFactory.createProvider(providerType, this.settings)
    }

    sendEmail(data) {
        const pFactory = new ProviderFactory()
        if (!this.emailProvider) {
            this.emailProvider = this.createProvider('email')
        }
        return this.emailProvider.send(data)
    }

    sendSms(data) {
        const pFactory = new ProviderFactory()
        if (!this.smsProvider) {
            this.smsProvider = this.createProvider('sms')
        }
        return this.smsProvider.send(data)
    }
}
