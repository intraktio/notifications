import Database from './database'

export default class Authentication {

  private db: Database;

  constructor(db: Database) {
    this.db = db;
  }

  auth(token: string) {
    let database = this.db;
    return new Promise(function(resolve, reject) {
      database.collection("accounts").where("token", "==", token).limit(1).get()
        .then(function(querySnapshot) {
                querySnapshot.forEach(function(doc) {
                    resolve(doc.data());
                });
            })
        .catch(err => {
          reject(err);
        });
    });
  };

}
