import Database from './database'
import ApiService from './api'
import NotificationsService from './notification-service'

export const Api = (basepath='', firebaseApp) => new ApiService(new Database(basepath, firebaseApp))
export const Notifications = (basepath='', firebaseApp) => new NotificationsService(new Database(basepath, firebaseApp))
