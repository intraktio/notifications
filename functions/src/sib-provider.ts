import Provider from './provider';

function parseSenderFromEmail(email) {
  try {
    let [_, fromName, fromEmail] = email.match(/(.*) <(.*)>/)
    return {
      name: fromName,
      email: fromEmail,
    }
  } catch (exp) {
    return {
      email: email
    }
  }
}

export class SIBProvider extends Provider {

  send(inp: any) {
      return new Promise((resolve, reject) => {
        const options = {
          method: 'POST',
          headers: {
            accept: 'application/json',
            'content-type': 'application/json',
            'api-key': this.accSettings.apiKey,
          },
          body: JSON.stringify({
            ...this.accSettings?.defaults,
            sender: parseSenderFromEmail(inp.from),
            to: [
              {email: inp.to}
            ],
            subject: inp.subject,
            htmlContent: inp.content,
            params: {body: inp.content},
            tags: inp.tags,
          })
        };
        console.log(options)

        fetch('https://api.sendinblue.com/v3/smtp/email', options)
          .then(response => response.json())
          .then(response => resolve(response))
          .catch(err => {
            reject(new Error("Error sending the email. Additional error output: " + err));
          });
      });
  }
}
