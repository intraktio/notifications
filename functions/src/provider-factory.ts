import {SMTPProvider} from './smtp-provider';
import {SIBProvider} from './sib-provider';
import {FreshdeskProvider} from './freshdesk-provider';
import {TwilioProvider} from './twilio-provider';

export default class ProviderFactory {

    createProvider(provider: string, accSettings: any) {
        if(provider == 'email') {
          switch(accSettings.emailProvider) {
            case 'freshdesk-mail':
              return new FreshdeskProvider(accSettings.providers['freshdesk-mail']);
            case 'smtpProvider':
              return new SMTPProvider(accSettings.providers.smtpProvider);
            case 'sendinblue-transactional-email':
                  return new SIBProvider(accSettings.providers['sendinblue-transactional-email']);
            default:
              throw new Error("Couldn't initialize a provider with the given emailProvider '" + accSettings.emailProvider + "'.");
          }
        } else if(provider == 'sms') {
          switch(accSettings.smsProvider) {
            case 'twilio':
              return new TwilioProvider(accSettings.providers.twilio);
            default:
              throw new Error("Couldn't initialize a provider with the given smsProvider '" + accSettings.smsProvider + "'.");
          }
        }
        throw new Error("Couldn't initialize a provider with the given providerName.");
    }
}
