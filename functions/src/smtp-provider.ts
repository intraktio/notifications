import Provider from './provider';
import * as nodemailer from 'nodemailer';

export class SMTPProvider extends Provider {

  send(body: any) {
      return new Promise((resolve, reject) => {
        let secure = false;
        if(this.accSettings.port == 465) secure = true;

        let transporter = nodemailer.createTransport({
          "host": this.accSettings.host,
          "port": this.accSettings.port,
          "secure": secure,
          "auth": {
            "user": this.accSettings.username,
            "pass": this.accSettings.password
          }
        });

        let data = {
          "from": body.from,
          "to": body.to,
          "subject": body.subject,
          "html": body.content,
        };

        transporter.sendMail(data, (error, info) => {
          if(error) {
            reject(new Error("Problem sending email with SendinBlue SMTP: "+error));
          }

          resolve(info);
        });
      });
  }
}
