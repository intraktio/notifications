import * as functions from 'firebase-functions';
import DatabaseLogger from './database-logger';
import ProviderFactory from './provider-factory';
import Authentication from './authenticate';
import Database from './database'
import NotificationService from './notification-service'
import 'es6-promise';

export default class Api {
    private db: Database

    constructor(db: Database) {
        this.db = db
    }

    sendEmail = functions.https.onRequest((req, res) => {
        const db = this.db
        const dbLogger = new DatabaseLogger(db);
        const auth = new Authentication(db);

        if(!dbLogger.log(req)) {
            res.send({"Success": false});
            return;
        }

        auth.auth(req.headers.authorization).then(accSettings => {
            const notifications = new NotificationService(db, accSettings)
            notifications.sendEmail(req.body).then(response => {
                console.log(response);
                res.send({"Success": true});
                return;
            }).catch(err => {
                res.send({"Success": false, "Error": err.message});
                return
            });
        }).catch(err => {
            res.send({"Success": false, "Error": err.message});
            return;
        });
    });

    sendSms = functions.https.onRequest((req, res) => {
        const db = this.db
        const dbLogger = new DatabaseLogger(db);
        const auth = new Authentication(db);

        if(!dbLogger.log(req)) {
            res.send({"Success": false});
            return;
        }

        auth.auth(req.headers.authorization).then(accSettings => {
            const notifications = new NotificationService(db, accSettings)
            notifications.sendSms(req.body).then(response => {
                console.log(response);
                res.send({"Success": true});
                return;
            }).catch(err => {
                res.send({"Success": false, "Error": err.message});
                return
            });
        }).catch(err => {
            res.send({"Success": false, "Error": err.message});
            return;
        });
    });
}
