export default abstract class Provider {

    protected accSettings: any;

    constructor(accSettings: any) {
      this.accSettings = accSettings;
    }

    abstract send(body: any): any;
}
