import Database from './database'

export default class DatabaseLogger {

  private db: Database;

  constructor(db: Database) {
    this.db = db;
  }

  log(req: any) {
    if(this.db==undefined) return false;

    const bodyFields = getFields(req.body);

    const headerFields = getFields(req.headers);

    // Log request
    try {
      this.db.collection("log").add({
          timestamp: new Date(),
          headers: headerFields,
          body: bodyFields
      });
    } catch(err) {
      return false;
    }

    return true;
  }

}

function getFields(data: any) {
  let fields = {};
  for(let key in data) {
    fields[key] = data[key];
  }

  return fields;
}
