import 'isomorphic-fetch';
import Provider from './provider';

export class FreshdeskProvider extends Provider {
  async send(reqBody: any) {
    let from = this.accSettings.defaultFrom;
    if(reqBody.from != undefined && reqBody.from != "") {
      from = reqBody.from;
    }
    return new Promise((resolve, reject) => {
      fetch(`https://${this.accSettings.domain}.freshdesk.com/api/v2/email_configs`, { // Get the email_config_id
        method: "GET",
        headers: {
          'Authorization': 'Basic ' + Buffer.from(this.accSettings.apiKey + ":X").toString('base64')
        }
      }).then(res => res.json())
      .then(resp => {
        if(resp.code) {
          reject(new Error("Couldn't get email configs (most likely a problem with your apiKey): " + resp.message));
          return;
        }
        let data = {
          "email": reqBody.to,
          "subject": reqBody.subject,
          "description": reqBody.content,
          "email_config_id": resp[0].id,
          "priority": 1
        };
        fetch(`https://${this.accSettings.domain}.freshdesk.com/api/v2/tickets/outbound_email`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + Buffer.from(this.accSettings.apiKey + ":X").toString('base64')
          },
          body: JSON.stringify(data)
        }).then(response => response.json())
        .then(resp => {
          if(resp.code) {
            reject(new Error("Error sending the email. Additional error output: " + resp.message));
            return;
          }
          resolve(resp);
        }).catch(err => {
          reject(new Error("Error sending the email. Additional error output: " + err));
        });
      }).catch(err => {
        reject(new Error("Couldn't fetch email configurations from freshdesk's api. Check that you have given the correct api key. Additional error output: " + err));
      });
    });
  }
}
