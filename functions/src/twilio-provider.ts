import Provider from './provider';
import * as Twilio from 'twilio';

export class TwilioProvider extends Provider {

    send(body: any) {
      return new Promise((resolve, reject) => {
        const twilioClient = Twilio(this.accSettings.sid, this.accSettings.authToken);

        let from = this.accSettings.defaultFrom;
        if(body.from != undefined && body.from != "") {
          from = body.from;
        }

        body.to.filter(this.validPhoneNumber).map(pn => {
          const messageContent = {
            body: body.content,
            to: pn,
            from: from
          }
          console.log("sending...");
          twilioClient.messages
            .create(messageContent)
            .then(message => resolve(message), err => reject(err));
        });
      });
    }

    validPhoneNumber(number: string) {
        return /^\+(?:[0-9] ?){6,14}[0-9]$/.test(number);
    }

}
